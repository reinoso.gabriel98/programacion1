package practica0;

import java.util.Scanner;

public class Cadenas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ejercicio17();
	}
	static void ejercicio16() {
		Scanner scan = new Scanner(System.in);
		System.out.println("ingrese un texto");
		String cadena = scan.nextLine();
		imprimirReversa(cadena);
	}
	static void imprimirReversa(String cadena) {
		System.out.println(reversa(cadena));
	}
	static String reversa(String cadena) {
		String anedac="";
		for(int x = cadena.length()-1;x>=0;x--) {
			anedac+=cadena.charAt(x);
		}
		return anedac;
	}
	static void ejercicio17() {
		Scanner scan = new Scanner(System.in);
		System.out.println("ingrese una cadena");
		String cadena = scan.nextLine();
		String chr = "";
		boolean ischr = false;
		System.out.println("ingrese un caracter");
		while(!ischr) {
			chr = scan.nextLine();
			if(chr.length()==1) {
				ischr= true;
			}else {
				System.out.println("solo se debe ingresar un solo caracter, ingrese un caracter");
			}
		}
		System.out.println("en la cadena '"+ cadena+"' se encuentran "+ cantidadApariciones(cadena, chr.charAt(0))+" veces el caracter '"+chr+"'");
	}
	static int cantidadApariciones(String s, char c) {
		int sum=0;
		for(int x = 0;x<s.length();x++) {
			if(s.charAt(x)==c) {
				sum++;
			}
		}
		return sum;
	}

}
